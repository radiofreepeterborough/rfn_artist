(function($, Drupal, drupalSettings) {

  window.playListHidden = 0;
  $('#artist-playlist-toggle').click( function() { 
      $('.artist-playlist-wrapper').slideToggle();
      if(window.playListHidden == 0 ) {

        window.playListHidden = 1;
        var linkText = $('#artist-playlist-close').html();
        $('#artist-playlist-toggle').html(linkText);
        var audioElements = document.getElementsByTagName('audio');
        audioElements[0].play();
      }
      else {

        window.playListHidden = 0;
        var linkText = $('#artist-playlist-open').html();
        $('#artist-playlist-toggle').html(linkText);
      }

})

  function playlistToggle() {
    $('artist-playlist-wrapper').slideToggle();
}


})(jQuery, Drupal, drupalSettings);

// Set up handler to move to the next track when track is done
var audioElements = document.getElementsByTagName('audio');
for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        audio.addEventListener('ended', playNext);
}

function playNext(event) {

    var thisElement = event.target;

    // Where is this player in the list? 
    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        if(audio == thisElement) {

            var next = i + 1;
            if( i >= audioElements.length - 1 ) {
                next = 0;
            }

            stopAllPlayers();
            audioElements[next].play();
            audioElements[next].scrollIntoView(false);
       }
    }
}


function stopAllOtherPlayers(event) {

    var audioElements = document.getElementsByTagName('audio');
    var thisElement = event.target;

    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        if(audio != thisElement) { // If this element is not the one that was clicked, stop aud

            audio.pause(); 
        }
    }
}

function stopAllPlayers() {

    var audioElements = document.getElementsByTagName('audio');
    var thisElement = event.target;

    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        audio.pause(); 
    }
}


