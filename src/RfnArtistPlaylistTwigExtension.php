<?php

namespace Drupal\rfn_artist;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Twig extension.
 */
class RfnArtistPlaylistTwigExtension extends \Twig_Extension {

  use StringTranslationTrait;

  /**
   * The storage handler class for nodes.
   *
   * @var \Drupal\node\NodeStorage
   */
  private $nodeStorage;

  /**
   * The storage handler class for nodes.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The rendererer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  private $renderer;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity
   *   The Entity type manager service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The Rendererer.
   */
  public function __construct(EntityTypeManagerInterface $entity, Renderer $renderer) {
    $this->nodeStorage = $entity->getStorage('node');
    $this->entityTypeManager = $entity;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
//  public function getFunctions() {
//    return [
//      new \Twig_SimpleFunction('playlist_for_artist', function ($artistNid = NULL) {
//
//        // $node = Node::load($artistNid);
//        $artistName = $node->getTitle();
//
//        $output = '';
//        $output .= '<a href="javascript://" id="artist-playlist-toggle"> &raquo; ' . $this->t('Click to listen to all tracks for %artistName', ['%artistName' => $artistName]) . "</a>";
//        $output .= '<div class="hidden" id="artist-playlist-close">&laquo;  ' . $this->t('Click to hide playlist for %artistName', ['%artistName' => $artistName]) . '</div>';
//        $output .= '<div class="hidden" id="artist-playlist-open">&raquo; ' . $this->t('Click to listen to all tracks for %artistName', ['%artistName' => $artistName]) . '</div>';
//
//        $output .= '<div class="artist-playlist-wrapper collapsed" style="display: none;">';
//
//        // Get all the recordings that belong to this artist.
//        $artistAlbumNodes = $this->entityTypeManager
//          ->getStorage('node')
//          ->loadByProperties(['field_artists' => $artistNid]);
//
//        $tracks = [];
//
//        foreach ($artistAlbumNodes as $album) {
//
//          foreach ($album->field_media_items as $track) {
//              $tracks[] = [
//                'title' => $track->entity->getTitle(),
//                'duration' => $track->entity->get('field_duration')->value ? $track->entity->get('field_duration')->value : '???',
//                'uri' => $track->entity->get('field_media_streaming_uri')->value,
//                'album' => $album->getTitle(),
//              ];
//          }
//        }
//
//        $renderable = [
//          '#theme' => 'artist_playlist',
//          '#artistName' => $artistName,
//          '#tracks' => $tracks,
//        ];
//
//        $output .= $this->renderer->renderPlain($renderable);
//
//        $output .= '</div>';
//        return $output;
//      }),
//    ];
//  }
//
//}
  public function getFunctions() {
//    return [
//      new \Twig_SimpleFunction('playlist_for_artist', function ($artistNid = NULL) {
//
//        // $node = Node::load($artistNid);
//        $artistName = $node->getTitle();
//
//        $output = '';
//        $output .= '<a href="javascript://" id="artist-playlist-toggle"> &raquo; ' . $this->t('Click to listen to all tracks for %artistName', ['%artistName' => $artistName]) . "</a>";
//        $output .= '<div class="hidden" id="artist-playlist-close">&laquo;  ' . $this->t('Click to hide playlist for %artistName', ['%artistName' => $artistName]) . '</div>';
//        $output .= '<div class="hidden" id="artist-playlist-open">&raquo; ' . $this->t('Click to listen to all tracks for %artistName', ['%artistName' => $artistName]) . '</div>';
//
//        $output .= '<div class="artist-playlist-wrapper collapsed" style="display: none;">';
//
//        // Get all the recordings that belong to this artist.
//        $artistAlbumNodes = $this->entityTypeManager
//          ->getStorage('node')
//          ->loadByProperties(['field_artists' => $artistNid]);
//
//        $tracks = [];
//
//        foreach ($artistAlbumNodes as $album) {
//
//          foreach ($album->field_media_items as $track) {
//              $tracks[] = [
//                'title' => $track->entity->getTitle(),
//                'duration' => $track->entity->get('field_duration')->value ? $track->entity->get('field_duration')->value : '???',
//                'uri' => $track->entity->get('field_media_streaming_uri')->value,
//                'album' => $album->getTitle(),
//              ];
//          }
//        }
//
//        $renderable = [
//          '#theme' => 'artist_playlist',
//          '#artistName' => $artistName,
//          '#tracks' => $tracks,
//        ];
//
//        $output .= $this->renderer->renderPlain($renderable);
//
//        $output .= '</div>';
//        return $output;
//      }),
//    ];
//  }
//
}